include Config.mk

################ Source files ########################################

SRCS	= $(wildcard *.cc)
OBJS	= $(SRCS:.cc=.o)

################ Compilation #########################################

.PHONY: all install uninstall
.PHONY: clean depend html check dist distclean maintainer-clean

all:	${EXE}

${EXE}:	${OBJS}
	@echo "Linking $@ ..."
	@${LD} ${LDFLAGS} -o $@ $^ ${LIBS}

%.o:	%.cc
	@echo "    Compiling $< ..."
	@${CXX} ${CXXFLAGS} -o $@ -c $<

%.s:	%.cc
	@echo "    Compiling $< to assembly ..."
	@${CXX} ${CXXFLAGS} -S -g0 -Os -DNDEBUG=1 -fomit-frame-pointer -o $@ -c $<

html:

################ Installation ########################################

INSTALLDIR	= ${INSTALL} -d
INSTALLBIN	= ${INSTALL} -s -p -m 755

install: ${EXE}
	@echo "Installing ${EXE} to ${BINDIR} ..."
	@${INSTALLDIR} ${BINDIR}
	@${INSTALLBIN} ${EXE} ${BINDIR}

uninstall:
	@echo "Removing ${EXE} from ${BINDIR} ..."
	@rm -f ${BINDIR}/${EXE}

################ Maintenance ###########################################

clean:
	@echo "Removing generated files ..."
	@rm -f ${EXE} ${OBJS} ${TOCLEAN}

depend: ${SRCS}
	@rm -f .depend
	@for i in ${SRCS}; do	\
	    ${CXX} ${CXXFLAGS} -M -MT $${i%%.cc}.o $$i >> .depend;	\
	done

TMPDIR	= /tmp
DISTDIR	= ${HOME}/stored
DISTNAM	= ${EXE}-${MAJOR}.${MINOR}
DISTTAR	= ${DISTNAM}.${BUILD}.tar.bz2

dist:
	mkdir ${TMPDIR}/${DISTNAM}
	cp -r . ${TMPDIR}/${DISTNAM}
	+make -C ${TMPDIR}/${DISTNAM} html distclean
	(cd ${TMPDIR}/${DISTNAM}; rm -rf `find . -name .svn`)
	(cd ${TMPDIR}; tar jcf ${DISTDIR}/${DISTTAR} ${DISTNAM}; rm -rf ${DISTNAM})

distclean:	clean
	@rm -f Config.mk config.h bsconf.o bsconf .depend

maintainer-clean: distclean
	@rm -rf docs/html

-include .depend
 
