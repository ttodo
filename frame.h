// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// frame.h
//

#ifndef FRAME_H_3C7C0B5325ECA7E7714C148D1DFE0331
#define FRAME_H_3C7C0B5325ECA7E7714C148D1DFE0331

#include "ilist.h"
#include "iedit.h"

/// \class CTodoFrame tdframe.h tdframe.h
class CTodoFrame : public CWindow {
public:
			CTodoFrame (void);
protected:
    virtual void	OnResize (rcrect_t wr);
    virtual void	OnInitialUpdate (void);
    virtual void	OnChildClose (uoff_t i);
    virtual void	OnKey (wchar_t key);
    virtual void	OnCommand (cmd_t c);
    virtual void	OnUpdateCommandUI (rcmd_t rc) const;
private:
    typedef CTodoList&		relist_t;
    typedef CItemEditDialog&	reedit_t;
    enum EPane {
	pane_MenuBar,
	pane_Entries,
	pane_Editor,
	pane_Last
    };
private:
    inline CMenuBar&	MenuBar (void)		{ return (TCW<CMenuBar>(pane_MenuBar)); }
    inline relist_t	TodoList (void)		{ return (TCW<CTodoList>(pane_Entries)); }
    inline reedit_t	ItemEditor (void)	{ return (TCW<CItemEditDialog>(pane_Editor)); }
};

#endif
