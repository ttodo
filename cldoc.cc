// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// cldoc.cc
//

#include "cldoc.h"
#include <unistd.h>
#include <time.h>

//----------------------------------------------------------------------

/// Default constructor.
CCurlistDocument::CCurlistDocument (void)
: CDocument (),
  m_List (),
  m_Deps (),
  m_Stack (),
  m_Selection ()
{
    m_Stack.push_back (c_RootItemId);
    m_Selection.push_back (0);
}

//--{ Internal data accessors }-----------------------------------------

/// Set selection to index \p v.
void CCurlistDocument::SetSelection (uoff_t v)
{
    m_Selection.back() = v;
    UpdateAllViews();
}

/// Sets selection to item \p id, if found.
void CCurlistDocument::SelectItem (itemid_t id)
{
    foreach (icitem_t, i, List())
	if (i->Id() == id)
	    SetSelection (distance (List().begin(), i));
}

/// Returns the currently selected item.
CCurlistDocument::rcitem_t CCurlistDocument::CurrentItem (void) const
{
    static const CTodoItem s_NullItem;
    return (Selection() < m_List.size() ? m_List[Selection()] : s_NullItem);
}

/// Returns the full path to the directory in containing the .todo file.
CCurlistDocument::rcfname_t CCurlistDocument::GetTodoFileDir (void) const
{
    static string dir;
    if (!dir.empty())
	return (dir);
    // Build the full path name of the supposed directory.
    if (Filename()[0] != '/') {
	char cwd [PATH_MAX];
	if (!getcwd (VectorBlock (cwd)))
	    throw libc_exception ("getcwd");
	dir = cwd;
    }
    dir += '/';
    dir += Document()->Filename();
    // Remove the trailing /.todo
    dir.erase (dir.iat (dir.rfind ('/')), dir.end());
    return (dir);
}

/// Returns true if the current item is a directory under the current item.
bool CCurlistDocument::ItemNameIsADirectory (rcfname_t name) const
{
    string dir (GetTodoFileDir());
    // Add the current item stack (skip the root item which is empty)
    for (itstack_t::const_iterator i = m_Stack.begin()+1; i < m_Stack.end(); ++i) {
	icitem_t ii = Document()->FindItem (*i);
	if (ii->Priority() != CTodoItem::priority_Directory)
	    return (false);	// The full path must be a directory
	dir += '/';
	dir += ii->Text();
    }
    dir += '/';
    dir += name;
    return (access (dir, F_OK) == 0);
}

/// Verifies that the item is or is not a directory as it says.
void CCurlistDocument::UpdateItemDirectoryFlag (iitem_t icuri) const
{
    if (ItemNameIsADirectory (icuri->Text()))
	icuri->SetPriority (CTodoItem::priority_Directory);
    else if (icuri->Priority() == CTodoItem::priority_Directory)
	icuri->SetPriority (CTodoItem::priority_Medium);
}

/// Uses the current working directory to find its sublist in the tree.
void CCurlistDocument::DescendByCwd (void)
{
    if ((ItemId() != c_RootItemId) | Flag(f_Descended))	// Only autodescend from root
	return;
    // Get current working directory
    char cwdbuf [PATH_MAX];
    if (!getcwd (VectorBlock (cwdbuf)))
	throw libc_exception ("getcwd");
    string cwd;
    cwd.link (cwdbuf, strlen (cwdbuf));
    // Determine the path to descend down
    uoff_t pathBase = cwd.size();
    const string& fn (Document()->Filename());
    SetFlag (f_Descended, !fn.empty());
    string dir;				// Parse fn for ../../../.todo
    for (uoff_t i = 0; i < fn.size(); i += dir.size() + 1) {
	dir.relink (fn.iat(i), min (fn.find ('/', i), fn.size()) - i);
	if (dir == "..")
	    pathBase = cwd.rfind ('/', pathBase-1);
	else if (dir != ".todo")	// If the user specified some .todo explicitly,
	    return;			// ... then don't descend.
    }
    // Now for each dir in the path see if there is a matching item
    for (uoff_t i = pathBase + 1; i < cwd.size(); i += dir.size() + 1) {
	dir.relink (cwd.iat(i), min (cwd.find ('/', i), cwd.size()) - i);
	foreach (icitem_t, ii, List()) {
	    if (ii->Complete())		// Directory items are never complete
		return;			// ... so stop looking at that point.
	    if (ii->Text() == dir) {	// Found the proper item, so descend
		SetSelection (distance (List().begin(), ii));
		EnterCurrentItem();
		break;
	    }
	}
    }
}

//--{ List manipulation }-----------------------------------------------

/// Called the first time the document is connected.
void CCurlistDocument::OnInitialUpdate (void)
{
    m_Stack.clear();
    m_Stack.push_back (c_RootItemId);
    m_Selection.clear();
    m_Selection.push_back (0);
    CDocument::OnInitialUpdate();
}

/// Copies visible data from the document.
void CCurlistDocument::OnUpdate (void)
{
    CDocument::OnUpdate();
    ReloadList();
    SetFlag (f_Changed, Document()->Flag (f_Changed));
    SetFlag (f_ReadOnly, Document()->Flag (f_ReadOnly));
    DescendByCwd();
}

/// Reloads the currently visible list from the overall data.
void CCurlistDocument::ReloadList (void)
{
    m_List.clear();
    // Find the list of dependencies for item lid
    Document()->ItemDeps (ItemId(), m_Deps);
    m_List.reserve (Deps().size());
    const time_t c_OneDay = 24 * 60 * 60;
    const time_t tooOld (time(NULL) - c_OneDay);
    // Insert all the items dependent on m_ItemId into m_List.
    foreach (icdep_t, d, Deps()) {
	iitem_t ii = Document()->FindItem (d->m_DepId);
	if (!Flag (f_CompleteVisible) && ii->Complete() && ii->Done() < tooOld)
	    break;
	m_List.push_back (*ii);
    }
    SetSelection (min (Selection(), uoff_t (m_List.size() - 1)));
}

//--{ Item editing }---------------------------------------------------

/// Sets the current item to \p e
void CCurlistDocument::UpdateCurrentItem (rcitem_t ni)
{
    assert (Selection() < List().size());
    iitem_t i (m_List.iat (Selection()));
    *i = ni;
    UpdateItemDirectoryFlag (i);
    SetFlag (f_Changed);
    Document()->UpdateItem (*i);
}

/// Creates a new item and selects it.
void CCurlistDocument::AppendItem (void)
{
    itemid_t id = Document()->CreateItem();
    Document()->LinkItem (id, ItemId());	// Calls update/reload
    SelectItem (id);
}

/// Makes \p id a dependency of the current item.
void CCurlistDocument::PasteLinkToItem (itemid_t id)
{
    // Create a dependency link to it from the current item.
    foreach (icdep_t, d, Deps())
	if (d->m_DepId == id)
	    return;	// Already in this list
    Document()->LinkItem (id, ItemId());	// Calls UpdateAllViews->ReloadList
    // List is reloaded at this point, so select the new item.
    SelectItem (id);
}

/// Removes the current item.
void CCurlistDocument::RemoveCurrentItem (void)
{
    Document()->UnlinkItem (m_Deps.iat (Selection()));
}

/// Marks the currently selected item as 100% complete.
void CCurlistDocument::MarkItemComplete (void)
{
    CTodoItem& i (m_List[Selection()]);
    i.MarkComplete (100 * !i.Complete());
    UpdateCurrentItem (i);
}

/// Sets the priority of the currently selected item to \p p.
void CCurlistDocument::SetCurrentItemPriority (CTodoItem::EPriority p)
{
    m_List[Selection()].SetPriority (p);
    UpdateCurrentItem (m_List[Selection()]);
}

/// Sets the currently active item as the current list.
void CCurlistDocument::EnterCurrentItem (void)
{
    rcitem_t i (CurrentItem());
    if (!i.Id())
	return;
    m_Stack.push_back (i.Id());
    m_Selection.push_back (0);
    ReloadList();
}

/// Goes up to the parent list of the current list.
void CCurlistDocument::LeaveCurrentItem (void)
{
    if (m_Stack.size() <= 1)
	return;
    m_Stack.pop_back();
    m_Selection.pop_back();
    ReloadList();
}

//--{ Command switching }-----------------------------------------------

/// Executes command \p c.
void CCurlistDocument::OnCommand (cmd_t c)
{
    CDocument::OnCommand (c);
    switch (c) {
	case cmd_Item_Complete:	MarkItemComplete();		break;
	case cmd_List_Leave:	LeaveCurrentItem();		break;
	case cmd_List_Enter:	EnterCurrentItem();		break;
	case cmd_List_OldItems:	ToggleCompleteVisible();	break;
	case cmd_Item_Delete:	RemoveCurrentItem();		break;
	case cmd_File_Save:	Document()->Save();		break;
	case cmd_Item_Priority_Highest:
	case cmd_Item_Priority_High:
	case cmd_Item_Priority_Medium:
	case cmd_Item_Priority_Low:
	case cmd_Item_Priority_Lowest:
	    SetCurrentItemPriority (CTodoItem::EPriority (c - cmd_Item_Priority_Highest + 1));
	    break;
	case cmd_File_Quit:
	    assert (Document()->Flag (f_Changed) == Flag (f_Changed));
	    if (Flag (f_Changed)) {
		int rv = MessageBox ("There are unsaved changes. Save now?", MB_YesNoCancel);
		if (rv == mbrv_Cancel)
		    return;
		else if (rv != mbrv_No)
		    Document()->Save();
	    }
	    CRootWindow::Instance().Close();
	    break;
    };
}

/// Updates visible flags of command \p rc.
void CCurlistDocument::OnUpdateCommandUI (rcmd_t rc) const
{
    CDocument::OnUpdateCommandUI (rc);
    const bool bHaveSelection (Selection() < List().size());
    const bool bReadOnly (Flag (f_ReadOnly));
    bool bActive = true;
    switch (rc.cmd) {
	case cmd_File_Save:	bActive = !bReadOnly && Flag(f_Changed);break;
	case cmd_List_Leave:	bActive = NestingDepth();		break;
	case cmd_List_OldItems:
	    rc.SetFlag (SCmd::cf_Checked, Flag (f_CompleteVisible));	break;
	case cmd_List_Enter:	bActive = bHaveSelection;		break;
	case cmd_Item_Delete:	bActive = bHaveSelection && !bReadOnly;	break;
	case cmd_Item_Complete:	bActive = bHaveSelection && !bReadOnly;	break;
	case cmd_Item_Priority:
	    bActive = bHaveSelection && CurrentItem().Priority() != CTodoItem::priority_Directory;
	    break;
	case cmd_Item_Priority_Highest:
	case cmd_Item_Priority_High:
	case cmd_Item_Priority_Medium:
	case cmd_Item_Priority_Low:
	case cmd_Item_Priority_Lowest:
	    bActive = bHaveSelection && !bReadOnly;
	    rc.SetFlag (SCmd::cf_Checked, rc.cmd == CurrentItem().Priority() + cmd_Item_Priority_Highest - 1);
	    break;
	default:
	    bActive = !(rc.flags & SCmd::cf_Grayed);
	    break;
    };
    rc.SetFlag (SCmd::cf_Grayed, !bActive);
}

