// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// tddoc.h
//

#ifndef TDDOC_H_31474BC008E2711A4DD60C960CFF3439
#define TDDOC_H_31474BC008E2711A4DD60C960CFF3439

#include "tde.h"

/// \class CTodoDocument tddoc.h tddoc.h
///
/// Contains the list of todo entries and the table of dependencies between
/// them. Provides accessors for modification of the list and the table.
///
class CTodoDocument : public CDocument {
public:
    typedef set<CTodoItem>		todoset_t;
    typedef const CTodoItem&		rcitem_t;
    typedef todoset_t::iterator		iitem_t;
    typedef todoset_t::const_iterator	icitem_t;
    typedef tddepmap_t::iterator	idep_t;
    typedef tddepmap_t::const_iterator	icdep_t;
    typedef CTodoItem::id_t		itemid_t;
    typedef const string&		rcfname_t;
public:
			CTodoDocument (void);
    virtual void	read (istream& is);
    virtual void	write (ostream& os) const;
    virtual size_t	stream_size (void) const;
    virtual void	Open (rcfname_t filename);
    virtual void	Save (void);
    icitem_t		FindItem (itemid_t id) const;
    inline iitem_t	FindItem (itemid_t id)		{ return (const_cast<iitem_t>(const_cast<const CTodoDocument*>(this)->FindItem(id))); }
    void		ItemDeps (itemid_t id, tddepmap_t& m) const;
    itemid_t		CreateItem (void);
    void		LinkItem (itemid_t id, itemid_t parent);
    void		UpdateItem (rcitem_t v);
    void		UnlinkItem (icdep_t id);
private:
    inline itemid_t	GetNextItemId (void) const;
    void		VerifyData (void);
    bool		VisibleOrderLess (const CTodoDep& d1, const CTodoDep& d2) const;
    void		ResortItemDeps (idep_t first, idep_t last) const;
    void		UpdateItemProgress (idep_t first, idep_t last);
    void		UpdateCompleteStatus (itemid_t dep);
private:
    todoset_t		m_Todos;	///< List of all entries.
    tddepmap_t		m_Deps;		///< List of dependencies between todos.
};

#endif

