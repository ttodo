// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// tde.cc
//

#include "tde.h"
#include <time.h>

/// Default constructor.
CTodoItem::CTodoItem (id_t id)
: m_Text (),
  m_Comment (),
  m_Epoch (0),
  m_Created (0),
  m_Started (0),
  m_Effort (0),
  m_Due (0),
  m_Done (0),
  m_Id (id),
  m_Priority (priority_Medium),
  m_Progress (0),
  m_bHasSublist (false),
  m_Refs (0)
{
    const time_t now (time (NULL));
    m_Created = uint32_t(now);
    m_Epoch = now >> 32;
}

/// Reads the object from stream \p is.
void CTodoItem::read (istream& is)
{
    is >> m_Epoch >> m_Created >> m_Started >> m_Effort >> m_Due >> m_Done
	>> m_Id >> m_Priority >> m_Progress >> m_bHasSublist >> m_Refs
	>> m_Text >> m_Comment >> ios::align(4);
}

/// Writes the object to stream \p os.
void CTodoItem::write (ostream& os) const
{
    os << m_Epoch << m_Created << m_Started << m_Effort << m_Due << m_Done
	<< m_Id << m_Priority << m_Progress << m_bHasSublist << m_Refs
	<< m_Text << m_Comment << ios::align(4);
}

/// Returns the size of the written object.
size_t CTodoItem::stream_size (void) const
{
    return (Align (stream_size_of (m_Epoch) +
		   stream_size_of (m_Created) +
		   stream_size_of (m_Started) +
		   stream_size_of (m_Effort) +
		   stream_size_of (m_Due) +
		   stream_size_of (m_Done) +
		   stream_size_of (m_Id) +
		   stream_size_of (m_Priority) +
		   stream_size_of (m_Progress) +
		   stream_size_of (m_bHasSublist) +
		   stream_size_of (m_Refs) +
		   stream_size_of (m_Text) +
		   stream_size_of (m_Comment), 4));
}

/// Marks the item's \p progress and sets the done time if appropriate.
void CTodoItem::MarkComplete (uint8_t progress)
{
    assert (progress <= 100 && "Progress field is a percentage");
    m_Progress = progress;
    if (m_Progress == 100)
	m_Done = time (NULL);
}

/// Returns text string containing the combined date.
const char* CTodoItem::TimeTText (uint32_t v) const
{
    time_t t (MakeTimeT (v));
    return (ctime (&t));
}

