// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// tde.h
//

#ifndef TDE_H_27699F347D16DF25781577481B852F4D
#define TDE_H_27699F347D16DF25781577481B852F4D

#include "config.h"

//----------------------------------------------------------------------

/// ToDo list entry.
class CTodoItem {
public:
    enum EPriority {
	priority_Directory,
	priority_Highest,
	priority_High,
	priority_Medium,
	priority_Low,
	priority_Lowest,
	priority_Last
    };
    typedef uint32_t		id_t;
    typedef const string&	rctext_t;
    typedef const CTodoItem&	rcself_t;
private:
    inline time_t	MakeTimeT (uint32_t v) const { return (sizeof(time_t) > 4 ? ((time_t(m_Epoch) << 32) | v) : v); }
    const char*		TimeTText (uint32_t v) const;
public:
			CTodoItem (id_t id = 0);
    void		read (istream& is);
    void		write (ostream& os) const;
    size_t		stream_size (void) const;
    void		MarkComplete (uint8_t progress = 100);
    inline uint32_t	Progress (void) const	{ return (m_Progress); }
    inline bool		Complete (void) const	{ return (Progress() == 100); }
    inline time_t	Created (void) const	{ return (MakeTimeT (m_Created)); }
    inline time_t	Done (void) const	{ return (MakeTimeT (m_Done)); }
    inline const char*	CreatedDateText (void) const { return (TimeTText (m_Created)); }
    inline const char*	DoneDateText (void) const { return (TimeTText (m_Done)); }
    inline id_t		Id (void) const		{ return (m_Id); }
    inline bool		HasSublist (void) const	{ return (m_bHasSublist); }
    inline void		SetHasSublist (bool v)	{ m_bHasSublist = v; }
    inline rctext_t	Text (void) const	{ return (m_Text); }
    inline rctext_t	Comment (void) const	{ return (m_Comment); }
    inline void		SetText (rctext_t v)	{ m_Text = v; }
    inline void		SetComment (rctext_t v)	{ m_Comment = v; }
    inline uint8_t	Refs (void) const	{ return (m_Refs); }
    inline bool		AddRef (void)		{ return (m_Refs < UINT8_MAX ? ++m_Refs : false); }
    inline bool		DelRef (void)		{ return (m_Refs > 0 ? --m_Refs : false); }
    inline uint8_t	Priority (void) const	{ return (m_Priority); }
    inline void		SetPriority (EPriority v)	{ m_Priority = v; }
    inline bool		operator< (rcself_t v) const	{ return (Id() < v.Id()); }
    inline bool		operator== (rcself_t v) const	{ return (Id() == v.Id()); }
private:
    string		m_Text;		///< Description of what to do.
    string		m_Comment;	///< Completion comment.
    uint32_t		m_Epoch;	///< Upper half of time_t for dates.
    uint32_t		m_Created;	///< When it was created.
    uint32_t		m_Started;	///< When work started.
    uint32_t		m_Effort;	///< How much work has been done on it.
    uint32_t		m_Due;		///< When it should be completed.
    uint32_t		m_Done;		///< When it was completed.
    id_t		m_Id;		///< Unique id of the entry.
    uint8_t		m_Priority;	///< Priority value. See #EPriority.
    uint8_t		m_Progress;	///< Percent complete.
    uint8_t		m_bHasSublist;	///< true if there are dependencies.
    uint8_t		m_Refs;		///< Number of dependent items.
};

//----------------------------------------------------------------------

/// Provides dependency links between todo items.
class CTodoDep {
public:
    typedef CTodoItem::id_t	itemid_t;
    typedef const CTodoDep&	rcself_t;
public:
			CTodoDep (itemid_t item = 0, itemid_t dep = 0) : m_ItemId (item), m_DepId (dep) { }
    inline bool		operator< (rcself_t v) const	{ return (m_ItemId < v.m_ItemId); }
    inline bool		operator== (rcself_t v) const	{ return (m_ItemId == v.m_ItemId); }
    inline void		read (istream& is)		{ is >> m_ItemId >> m_DepId; }
    inline void		write (ostream& os) const	{ os << m_ItemId << m_DepId; }
    inline size_t	stream_size (void) const	{ return (stream_size_of (m_ItemId) + stream_size_of (m_DepId)); }
public:
    itemid_t	m_ItemId;	///< Item id that has ...
    itemid_t	m_DepId;	///< ... this dependency.
};

//----------------------------------------------------------------------

enum { c_RootItemId };

//----------------------------------------------------------------------

typedef vector<CTodoItem>	tdevec_t;
typedef vector<CTodoDep>	tddepmap_t;

STD_STREAMABLE (CTodoItem)
STD_STREAMABLE (CTodoDep)

#endif

