// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// iedit.cc
//

#include "iedit.h"

/// Default constructor.
CItemEditDialog::CItemEditDialog (void)
: CDialog (),
  m_Item ()
{
    AddChild (new CEditBox);
    AddChild (new CLabel);
    AddChild (new CLabel);
    assert (Children().size() == ctrl_Last);
}

void CItemEditDialog::OnUpdate (void)
{
    CDialog::OnUpdate();
    SetItem (Document()->CurrentItem());
}

void CItemEditDialog::OnResize (rcrect_t wr)
{
    CDialog::OnResize (wr);
    ItemText().OnResize	(Rect (1, 1, wr.Width() - 2, 1));
    CreatedDate().OnResize	(Rect (wr.Width() - 26, 3, 25, 1));
    DoneDate().OnResize		(Rect (wr.Width() - 26, 4, 25, 1));
}

void CItemEditDialog::OnKey (wchar_t key)
{
    CDialog::OnKey (key);
    if (ItemText().Flag (f_OffersFocus)) {
	if (ItemText().Flag (f_Changed)) {
	    m_Item.SetText (ItemText().Text());
	    SetFlag (f_Changed);
	}
	SetFlag (f_OffersFocus);
	if (Flag (f_Changed))
	    Document()->UpdateCurrentItem (Item());
	else if (Document()->CurrentItem().Text().empty())
	    Document()->RemoveCurrentItem();
    }
}

void CItemEditDialog::SetItem (rcentry_t e)
{
    m_Item = e;
    ItemText().SetText (e.Text());
    CreatedDate().SetText (e.CreatedDateText());
    DoneDate().SetText (e.Complete() ? e.DoneDateText() : "Active");
    SetFocus (ctrl_Text);
    ClearFlag (f_Changed);
}

