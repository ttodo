// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// ilist.cc
//

#include "ilist.h"
#include <stdio.h>

//----------------------------------------------------------------------

const CTodoList::SCommandKey CTodoList::c_CmdKeys[] = {
    { 'q',		cmd_File_Quit		},
    { 'S',		cmd_File_Save		},
    { 'l',		cmd_List_Enter		},
    { kv_Enter,		cmd_List_Enter		},
    { kv_Right,		cmd_List_Enter		},
    { kv_Left,		cmd_List_Leave		},
    { 'h',		cmd_List_Leave		},
    { 'o',		cmd_List_OldItems	},
    { 'c',		cmd_List_Copy		},
    { 'v',		cmd_List_Paste		},
    { kv_Insert,	cmd_Item_New		},
    { 'n',		cmd_Item_New		},
    { kv_Delete,	cmd_Item_Delete		},
    { 'D',		cmd_Item_Delete		},
    { kv_Space,		cmd_Item_Complete	},
    { 'e',		cmd_Item_Edit		},
    { '1',	cmd_Item_Priority_Highest	},
    { '2',	cmd_Item_Priority_High		},
    { '3',	cmd_Item_Priority_Medium	},
    { '4',	cmd_Item_Priority_Low		},
    { '5',	cmd_Item_Priority_Lowest	}
};

/// Constructs a list displaying \p rctdl
CTodoList::CTodoList (void)
: CListbox (),
  m_pTodos (NULL),
  m_CopiedId (0)
{
    SetCommandKeys (VectorBlock (c_CmdKeys));
}

/// Sets the list to display.
void CTodoList::SetList (pctdevec_t pl)
{
    if ((m_pTodos = pl))
	SetListSize (m_pTodos->size());
}

/// Sets internal variables when the document is updated.
void CTodoList::OnUpdate (void)
{
    CListbox::OnUpdate();
    SetList (&Document()->List());
    SetSelection (Document()->Selection());
}

/// Draws list item \p ii at \p pos onto \p gc.
void CTodoList::OnDrawItem (CGC& gc, rcpos_t pos, uint32_t ii)
{
    CListbox::OnDrawItem (gc, pos, ii);
    if (!m_pTodos)
	return;
    const CTodoItem& e ((*m_pTodos)[ii]);
    char progBuf [5];
    gc.Char (pos, "+ "[!e.HasSublist()]);
    snprintf (progBuf, 5, "%3u%%", e.Progress());
    gc.Text (pos[0] + 1, pos[1], e.Progress() ? progBuf : "    ");
    static const EColor c_PriorityColors [2][CTodoItem::priority_Last] = {
	{ white, lightred, yellow, green, cyan, lightblue },
	{ white, lightred, yellow, lightgreen, lightcyan, lightblue }
    };
    gc.FgColor (c_PriorityColors [ii == Selection()][e.Priority()]);
    gc.Text (pos[0] + 6, pos[1], e.Text());
}

/// Causes the current entry to be edited.
inline void CTodoList::EditItem (void)
{
    SetFlag (f_OffersFocus);
}

/// Processes keystroke \p key.
void CTodoList::OnKey (wchar_t key)
{
    CListbox::OnKey (key);
    if (Document()->Selection() != Selection())
	Document()->SetSelection (Selection());
}

/// Executes command \p c.
void CTodoList::OnCommand (cmd_t c)
{
    CListbox::OnCommand (c);
    pdoc_t pDoc = Document();
    switch (c) {
	case cmd_Item_New:	pDoc->AppendItem();
	case cmd_Item_Edit:	EditItem();				break;
	case cmd_List_Copy:	m_CopiedId = pDoc->CurrentItem().Id();	break;
	case cmd_List_Paste:	pDoc->PasteLinkToItem (m_CopiedId);	break;
    };
}

void CTodoList::OnUpdateCommandUI (rcmd_t rc) const
{
    CListbox::OnUpdateCommandUI (rc);
    pcdoc_t pDoc = Document();
    const bool bHaveSelection (pDoc->Selection() < pDoc->List().size());
    const bool bReadOnly (Flag (f_ReadOnly));
    bool bActive = true;
    switch (rc.cmd) {
	case cmd_Item_New:						break;
	case cmd_Item_Edit:	bActive = bHaveSelection && !bReadOnly;	break;
	case cmd_List_Copy:	bActive = bHaveSelection;		break;
	case cmd_List_Paste:	bActive = !bReadOnly && m_CopiedId;	break;
	default:
	    bActive = !(rc.flags & SCmd::cf_Grayed);
	    break;
    };
    rc.SetFlag (SCmd::cf_Grayed, !bActive);
}

