// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// ilist.h
//

#ifndef ILIST_H_7BDB6D9539FC19CC7C7DA4A0140965B0
#define ILIST_H_7BDB6D9539FC19CC7C7DA4A0140965B0

#include "cldoc.h"

/// List of todo entries.
class CTodoList : public CListbox {
public:
    typedef const tdevec_t*	pctdevec_t;
public:
			CTodoList (void);
    void		SetList (pctdevec_t pl);
    virtual void	OnDrawItem (CGC& gc, rcpos_t pos, uint32_t ii);
    virtual void	OnKey (wchar_t key);
protected:
    virtual void	OnUpdate (void);
    virtual void	OnCommand (cmd_t c);
    virtual void	OnUpdateCommandUI (rcmd_t rc) const;
private:
    typedef CCurlistDocument*	pdoc_t;
    typedef const CCurlistDocument* pcdoc_t;
    typedef CTodoItem::id_t	itemid_t;
private:
    inline pdoc_t	Document (void)		{ return (TDocument<CCurlistDocument>()); }
    inline pcdoc_t	Document (void) const	{ return (TDocument<CCurlistDocument>()); }
    inline void		EditItem (void);
private:
    pctdevec_t		m_pTodos;	///< Link to the data.
    itemid_t		m_CopiedId;	///< Cut-n-paste link buffer.
    static const SCommandKey c_CmdKeys[];
};

#endif

