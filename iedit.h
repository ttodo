// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// iedit.h
//

#ifndef IEDIT_H_2D55AEF271DA70232C96241A36B8EA24
#define IEDIT_H_2D55AEF271DA70232C96241A36B8EA24

#include "cldoc.h"

/// \class CItemEditDialog editdlg.h editdlg.h
class CItemEditDialog : public CDialog {
public:
    typedef const CTodoItem&	rcentry_t;
public:
			CItemEditDialog (void);
    void		SetItem (rcentry_t e);
    inline rcentry_t	Item (void) const	{ return (m_Item); }
    virtual void	OnResize (rcrect_t wr);
protected:
    virtual void	OnKey (wchar_t key);
    virtual void	OnUpdate (void);
private:
    enum {
	ctrl_Text,
	ctrl_CreatedDate,
	ctrl_DoneDate,
	ctrl_Last
    };
    typedef CCurlistDocument*	pdoc_t;
    typedef const CCurlistDocument* pcdoc_t;
private:
    inline pdoc_t	Document (void)		{ return (TDocument<CCurlistDocument>()); }
    inline pcdoc_t	Document (void) const	{ return (TDocument<CCurlistDocument>()); }
    inline CEditBox&	ItemText (void)	{ return (TCW<CEditBox>(ctrl_Text)); }
    inline CLabel&	CreatedDate (void)	{ return (TCW<CLabel>(ctrl_CreatedDate)); }
    inline CLabel&	DoneDate (void)		{ return (TCW<CLabel>(ctrl_DoneDate)); }
private:
    CTodoItem		m_Item;
};

#endif
