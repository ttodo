// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// cldoc.h
//

#ifndef CLDOC_H_0C4B8F6A2D8E432948411FC661E0CABB
#define CLDOC_H_0C4B8F6A2D8E432948411FC661E0CABB

#include "tddoc.h"

/// \class CCurlistDocument cldoc.h cldoc.h
class CCurlistDocument : public CDocument {
public:
    typedef const tdevec_t&	rctdevec_t;
    typedef const CTodoItem&	rcitem_t;
    typedef CTodoItem::id_t	itemid_t;
    typedef const string&	rcfname_t;
    enum {
	f_CompleteVisible = CDocument::f_Last,	///< All old items are visible.
	f_Descended,				///< Descended down item tree by dir path.
	f_Last
    };
public:
			CCurlistDocument (void);
    rcitem_t		CurrentItem (void) const;
    inline uint32_t	Selection (void) const	{ return (m_Selection.back()); }
    inline rctdevec_t	List (void) const	{ return (m_List); }
    void		SetSelection (uoff_t v);
    void		AppendItem (void);
    void		PasteLinkToItem (itemid_t id);
    void		UpdateCurrentItem (rcitem_t e);
    void		RemoveCurrentItem (void);
private:
    inline void		ToggleCompleteVisible (void)	{ SetFlag (f_CompleteVisible, !Flag (f_CompleteVisible)); ReloadList(); }
    inline size_t	NestingDepth (void) const	{ return (m_Stack.size() - 1); }
    void		SetCurrentItemPriority (CTodoItem::EPriority p);
    void		MarkItemComplete (void);
    void		EnterCurrentItem (void);
    void		LeaveCurrentItem (void);
protected:
    virtual void	OnUpdate (void);
    virtual void	OnInitialUpdate (void);
    virtual void	OnCommand (cmd_t c);
    virtual void	OnUpdateCommandUI (rcmd_t rc) const;
private:
    typedef CTodoDocument*		pdoc_t;
    typedef const CTodoDocument*	pcdoc_t;
    typedef tdevec_t::iterator		iitem_t;
    typedef tdevec_t::const_iterator	icitem_t;
    typedef tddepmap_t::iterator	idep_t;
    typedef tddepmap_t::const_iterator	icdep_t;
    typedef const tddepmap_t&		rcdepmap_t;
    typedef vector<itemid_t>		itstack_t;
    typedef vector<uint32_t>		selstack_t;
private:
    inline pdoc_t	Document (void)		{ return (TDocument<CTodoDocument>()); }
    inline pcdoc_t	Document (void) const	{ return (TDocument<CTodoDocument>()); }
    inline rcdepmap_t	Deps (void) const	{ return (m_Deps); }
    void		DescendByCwd (void);
    bool		ItemNameIsADirectory (rcfname_t name) const;
    void		UpdateItemDirectoryFlag (iitem_t icuri) const;
    void		ReloadList (void);
    void		SelectItem (itemid_t id);
    inline itemid_t	ItemId (void) const	{ return (m_Stack.back()); }
    rcfname_t		GetTodoFileDir (void) const;
private:
    tdevec_t		m_List;		///< Currently selected list.
    tddepmap_t		m_Deps;		///< Dependency list that created m_List.
    itstack_t		m_Stack;	///< The callstack of the currently active list.
    selstack_t		m_Selection;	///< Index of the selected item.
};

//----------------------------------------------------------------------

/// Global commands for this application.
enum {
    cmd_List = cmd_User,
    cmd_List_Enter,
    cmd_List_Leave,
    cmd_List_Copy,
    cmd_List_Paste,
    cmd_List_OldItems,
    cmd_Item,
    cmd_Item_New,
    cmd_Item_Edit,
    cmd_Item_Delete,
    cmd_Item_Complete,
    cmd_Item_Priority,
    cmd_Item_Priority_Highest,
    cmd_Item_Priority_High,
    cmd_Item_Priority_Medium,
    cmd_Item_Priority_Low,
    cmd_Item_Priority_Lowest,
};

//----------------------------------------------------------------------

#endif

