// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// frame.cc
//

#include "frame.h"
#include "tddoc.h"

//----------------------------------------------------------------------

BEGIN_MENU(4, c_TodoMenu)
    SUBMENU(2, "&File",	cmd_File),
	MENUITEM ("&Save",	cmd_File_Save),
	MENUITEM ("&Quit",	cmd_File_Quit),
    SUBMENU(7, "&List",	cmd_List),
	MENUITEM ("&Enter",	cmd_List_Enter),
	MENUITEM ("&Leave",	cmd_List_Leave),
	MENUITEM_SEPARATOR,
	MENUITEM ("&Copy",	cmd_List_Copy),
	MENUITEM ("&Paste",	cmd_List_Paste),
	MENUITEM_SEPARATOR,
	MENUCHECK ("&Old items",cmd_List_OldItems),
    SUBMENU(6, "&Item",	cmd_Item),
	MENUITEM ("&New",	cmd_Item_New),
	MENUITEM ("&Edit",	cmd_Item_Edit),
	MENUITEM ("&Delete",	cmd_Item_Delete),
	MENUITEM_SEPARATOR,
	MENUITEM ("&Complete",	cmd_Item_Complete),
	SUBMENU(5,"&Priority",	cmd_Item_Priority),
	    MENUCHECK ("H&ighest",	cmd_Item_Priority_Highest),
	    MENUCHECK ("&High",		cmd_Item_Priority_High),
	    MENUCHECK ("&Medium",	cmd_Item_Priority_Medium),
	    MENUCHECK ("&Low",		cmd_Item_Priority_Low),
	    MENUCHECK ("Low&est",	cmd_Item_Priority_Lowest),
    SUBMENU(2, "&Help",	cmd_Help),
	MENUITEM ("&Contents",	cmd_Help_Contents),
	MENUITEM ("&About",	cmd_Help_About)
END_MENU

//----------------------------------------------------------------------

/// Default constructor.
CTodoFrame::CTodoFrame (void)
: CWindow ()
{
    AddChild (CMenuBar::Instance());
    AddChild (new CTodoList);
    AddChild (new CItemEditDialog);
    assert (pane_Last == Children().size());
    CMenuBar::Instance()->Set (VectorBlock (c_TodoMenu));
}

/// Registers each child pane with the document.
void CTodoFrame::OnInitialUpdate (void)
{
    CWindow::OnInitialUpdate();
    Document()->RegisterView (&TodoList());
    Document()->RegisterView (&ItemEditor());
}

/// Allows each child pane to close the frame by closing itself.
void CTodoFrame::OnChildClose (uoff_t i)
{
    CWindow::OnChildClose (i);
    Close (CW(i).Status());
}

/// Resizes and places child windows.
void CTodoFrame::OnResize (rcrect_t wr)
{
    CWindow::OnResize (wr);
    Rect menuRect (wr);
    CW(pane_MenuBar).SizeHints (menuRect);
    MenuBar().OnResize (menuRect);
    TodoList().OnResize (Rect (0, menuRect.Height(), wr.Width(), wr.Height() - 8 - menuRect.Height()));
    ItemEditor().OnResize (Rect (0, wr.Height() - 8, wr.Width(), 8));
}

void CTodoFrame::OnKey (wchar_t key)
{
    if (key & kvm_Alt)
	SetFocus (pane_MenuBar);
    CWindow::OnKey (key);
}

void CTodoFrame::OnCommand (cmd_t c)
{
    Document()->OnCommand (c);
    CWindow::OnCommand (c);
}

void CTodoFrame::OnUpdateCommandUI (rcmd_t rc) const
{
    rc.SetFlag (SCmd::cf_Grayed, !Document());
    if (!Document())
	return;
    Document()->OnUpdateCommandUI (rc);
    CWindow::OnUpdateCommandUI (rc);
}

