// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// todo.h
//

#ifndef TODO_H_2485BD5D6B62EC7E1E1B95096F9679E7
#define TODO_H_2485BD5D6B62EC7E1E1B95096F9679E7

#include "cldoc.h"

class CTodoApp : public CApplication {
public:
    static CTodoApp&	Instance (void);
protected:
    void		OnCreate (argc_t argc, argv_t argv);
private:
			CTodoApp (void);
    void		GetDefaultFilename (string& filename) const;
private:
    CTodoDocument	m_Doc;
    CCurlistDocument	m_Curlist;
};

#endif

