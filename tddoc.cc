// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// tddoc.cc
//

#include "tddoc.h"
#include <unistd.h>
#if HAVE_IFF_H
    #include <iff.h>
#else
    #error "libiff-0.1.0 or later is required. http://iff.sourceforge.net"
#endif
#if !defined(IFF_VERSION) || IFF_VERSION < 0x010
    #error "libiff-0.1.0 or later is required. http://iff.sourceforge.net"
#endif

//----------------------------------------------------------------------

/// Default constructor.
CTodoDocument::CTodoDocument (void)
: CDocument (),
  m_Todos (),
  m_Deps ()
{
    m_Todos.insert (CTodoItem());
}

//--{ Internal data accessors }-----------------------------------------

/// Looks up item \p id in m_Todos, returns NULL if not found.
CTodoDocument::icitem_t CTodoDocument::FindItem (itemid_t id) const
{
    icitem_t i = m_Todos.find (id);
    return (i == m_Todos.end() ? NULL : i);
}

/// Links \p m to the dependency list for \p id.
void CTodoDocument::ItemDeps (itemid_t id, tddepmap_t& m) const
{
    pair<icdep_t, icdep_t> dr = equal_range (m_Deps, CTodoDep(id));
    m.link (dr.first, dr.second);
}

/// Returns the next available item id.
inline CTodoDocument::itemid_t CTodoDocument::GetNextItemId (void) const
{
    return (m_Todos.back().Id() + 1);
}

//--{ Serialization }---------------------------------------------------

enum {
    fmt_TodoList	= IFF_FMT('T','O','D','O'),
    fmt_Options		= IFF_FMT('O','P','T','S'),
    fmt_Item		= IFF_FMT('T','I','T','M'),
    fmt_Dep		= IFF_FMT('T','D','E','P')
};

static const uint32_t c_CurrentVersion	= 2;

//----------------------------------------------------------------------

class CTodoHeader {
public:
    inline		CTodoHeader (void)		: m_Version (c_CurrentVersion), m_Flags (0), m_Reserved (0) { }
    inline void		read (istream& is)		{ is >> m_Version >> m_Flags >> m_Reserved; }
    inline void		write (ostream& os) const	{ os << m_Version << m_Flags << m_Reserved; }
    inline size_t	stream_size (void) const	{ return (stream_size_of (m_Version) + stream_size_of (m_Flags) + stream_size_of(m_Reserved)); }
public:
    uint32_t		m_Version;	///< File format's version number.
    bitset<32>		m_Flags;	///< Various flags. None for now.
    uint32_t		m_Reserved;	///< Future functionality.
};

STD_STREAMABLE (CTodoHeader)

/// Reads the object from stream \p is.
void CTodoDocument::read (istream& is)
{
    iff::SkipChunk (is, fmt_Options);
    iff::ReadVector (is, m_Todos, fmt_Item);
    iff::ReadVector (is, m_Deps, fmt_Dep);
    VerifyData();
}

/// Writes the object to stream \p os.
void CTodoDocument::write (ostream& os) const
{
    iff::WriteChunk (os, CTodoHeader(), fmt_Options);
    iff::WriteVector (os, m_Todos, fmt_Item);
    iff::WriteVector (os, m_Deps, fmt_Dep);
}

/// Returns the size of the written object.
size_t CTodoDocument::stream_size (void) const
{
    return (iff::chunk_size_of (CTodoHeader()) +
	    iff::vector_size_of (m_Todos) +
	    iff::vector_size_of (m_Deps));
}

/// Opens \p filename and reads todo entries from it.
void CTodoDocument::Open (const string& filename)
{
    CDocument::Open (filename);
    if (access (filename, F_OK))	// Check if it's a new document.
	return;
    if (access (filename, W_OK))	// Check if it is read-only.
	SetFlag (f_ReadOnly);
    memblock buf;
    buf.read_file (filename);
    istream is (buf);
    iff::ReadFORM (is, *this, fmt_TodoList);
    SetFlag (f_Changed, false);
    UpdateAllViews();
}

/// Saves the data to the currently open file.
void CTodoDocument::Save (void)
{
    if (Flag (f_ReadOnly) && access (Filename(), W_OK)) {
	MessageBox ("Can't save: this file is marked as read-only");
	return;
    }
    memblock buf (iff::form_size_of (*this));
    ostream os (buf);
    iff::WriteFORM (os, *this, fmt_TodoList);
    buf.write_file (Filename());
    SetFlag (f_Changed, false);
    UpdateAllViews();
}

/// Verifies and corrects any defects in the data.
void CTodoDocument::VerifyData (void)
{
    foreach (tddepmap_t::iterator, i, m_Deps) {
	if (!FindItem (i->m_ItemId) || !FindItem (i->m_DepId)) {
	    assert (!"Found a dependency pointing to a nonexistent item!");
	    --(i = m_Deps.erase (i));
	}
    }
}

/// Returns the true if \p i1 ought to appear before \p i2 in the visible list.
bool CTodoDocument::VisibleOrderLess (const CTodoDep& d1, const CTodoDep& d2) const
{
    const icitem_t i1 (FindItem (d1.m_DepId)), i2 (FindItem (d2.m_DepId));
    // Sort to put completed items at the end, then sort by priority, and then by creation date.
    return (i1->Complete() < i2->Complete() ||
	    (!i1->Complete() && !i2->Complete() && (i1->Priority() < i2->Priority() ||
	     (i1->Priority() == i2->Priority() && *i1 < *i2))) ||
	    (i1->Complete() && i2->Complete() && (i1->Done() > i2->Done() ||
	     (i1->Done() == i2->Done() && *i1 < *i2))));
}

/// Reorders the items in the list by canonical sort order.
void CTodoDocument::ResortItemDeps (idep_t first, idep_t last) const
{
    for (idep_t j, i = first; ++i < last;) {
	for (j = i; j-- > first && !VisibleOrderLess (*j, *i););
	rotate (++j, i, i + 1);
    }
}

//--{ Item progress recursive updating }--------------------------------

/// Creates a new item and returns its id.
CTodoDocument::itemid_t CTodoDocument::CreateItem (void)
{
    CTodoItem e (GetNextItemId());
    assert (!FindItem (e.Id()));
    // To the complete list
    m_Todos.insert (e);
    SetFlag (f_Changed);
    return (e.Id());
}

/// Makes item \p id a dependency of \p parent item.
void CTodoDocument::LinkItem (itemid_t id, itemid_t parent)
{
    iitem_t iItem = FindItem (id), iParent = FindItem (parent);
    if (!iParent | !iItem)
	return;
    // m_Deps is sorted by parent, but not by < (because < requires accessing m_Todos)
    idep_t ip = upper_bound (m_Deps, CTodoDep (parent));
    m_Deps.insert (ip, CTodoDep (parent, id));
    // Update the new item's status.
    iItem->AddRef();
    iParent->SetHasSublist (true);
    UpdateCompleteStatus (id);
    // Tell the world
    SetFlag (f_Changed);
    UpdateAllViews();
}

/// Removes one reference the given item.
void CTodoDocument::UnlinkItem (icdep_t id)
{
    assert (id >= m_Deps.begin() && id < m_Deps.end() && "UnlinkItem takes an iterator from the vector set by ItemDeps");
    iitem_t ii = FindItem (id->m_DepId);	// Cache master list iterator while we still have the item to look for.
    // Remove from dependency list.
    m_Deps.erase (const_cast<idep_t>(id));
    pair<idep_t, idep_t> r = equal_range (m_Deps, CTodoDep(ii->Id()));
    if (r.first == r.second)	// Can't be cleanly done in UpdateItemProgress.
	FindItem (id->m_ItemId)->SetHasSublist (false);
    // Update the item in the master list.
    if (!ii->DelRef())	// If no more refs, then delete.
	m_Todos.erase (ii);
    UpdateItemProgress (r.first, r.second);
    // Update visible list.
    SetFlag (f_Changed);
    UpdateAllViews();
}

/// Updates item \p v in the master list.
void CTodoDocument::UpdateItem (rcitem_t v)
{
    iitem_t i = FindItem (v.Id());
    assert (i && "Update item must only be called with already existent items. Call AppendItem to create new ones.");
    *i = v;
    UpdateCompleteStatus (v.Id());
    // Tell the world
    SetFlag (f_Changed);
    UpdateAllViews();
}

/// Updates the progress of the item given by dependency range [first, last)
void CTodoDocument::UpdateItemProgress (idep_t first, idep_t last)
{
    const uint32_t nItems = distance (first, last);
    if (!nItems)
	return;
    ResortItemDeps (first, last);
    const itemid_t rangeId (first->m_ItemId);
    uint32_t progress = 0;
    for (; first < last; ++first)
	progress += FindItem(first->m_DepId)->Progress();
    // +1 treats the parent item as part of the list.
    progress = min (progress / (nItems + 1), 100U);
    iitem_t ii = FindItem (rangeId);
    if (!ii || ii->Progress() == progress)
	return;
    ii->MarkComplete (progress);
    UpdateCompleteStatus (rangeId);	// Recurse to propagate the change up.
}

/// Updates progress values of item dependent on \p dep
void CTodoDocument::UpdateCompleteStatus (itemid_t dep)
{
    // Each item has a range of dependencies; ranges are sequential in m_Deps.
    idep_t rangeStart = m_Deps.begin();	// The start of the current range.
    const idep_t iEnd = m_Deps.end();
    bool hasDep = false;		// Does current range have dep?
    for (idep_t i = rangeStart; i < iEnd; ++i) {
	if (i->m_ItemId != rangeStart->m_ItemId) {
	    if (hasDep)
		UpdateItemProgress (rangeStart, i);
	    rangeStart = i;
	    hasDep = false;
	}
	hasDep = hasDep || i->m_DepId == dep;
    }
    if (hasDep)
	UpdateItemProgress (rangeStart, iEnd);
}

