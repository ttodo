// This file is part of a terminal todo application.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// todo.cc
//

#include "todo.h"
#include "frame.h"
#include <unistd.h>

//----------------------------------------------------------------------

/// Default constructor.
CTodoApp::CTodoApp (void)
: CApplication (),
  m_Doc (),
  m_Curlist ()
{
}

/// Singleton interface.
/*static*/ CTodoApp& CTodoApp::Instance (void)
{
    static CTodoApp s_App;
    return (s_App);
}

/// Set \p filename to a default value to be used when none is given on the cmdline.
void CTodoApp::GetDefaultFilename (string& filename) const
{
    filename.clear();
    //
    // This code gets the first available .todo in the current path.
    // 	That is, it will use ./.todo, or ../.todo, etc.
    //
    // Get the nesting level to know how far back to check.
    char cwd [PATH_MAX];
    if (!getcwd (VectorBlock (cwd)))
	throw libc_exception ("getcwd");
    const size_t nDirs = count (cwd, cwd + strlen(cwd), '/');
    assert (nDirs && "A path without root in it?");

    // Check for existing .todo in each directory.
    filename.reserve (strlen("../") * nDirs + strlen(".todo"));
    int i;
    for (i = 0; i < int(nDirs); ++i)
	filename += "../";
    filename += ".todo";
    for (; i >= 0; --i)
	if (access (filename.iat (i * strlen("../")), F_OK) == 0)
	    break;

    // If there are no .todo files, create ./.todo
    if (i < 0) {
	i = nDirs - 1;
	if (access (".", W_OK) != 0) {
	    MessageBox ("You are not allowed to write to this directory");
	    CRootWindow::Instance().Close();
	}
    }
    filename.erase (filename.begin(), i * strlen("../"));
}

/// Initializes the output device.
void CTodoApp::OnCreate (argc_t argc, argv_t argv)
{
    CApplication::OnCreate (argc, argv);
    if (argc > 2) {
	cerr << "Usage: todo [filename]\n";
	return (CRootWindow::Instance().Close());
    }

    // Determine database name.
    string fullname;
    if (argc == 2)
	fullname = argv[1];
    else
	GetDefaultFilename (fullname);

    // Create the frame and register the document with it.
    CTodoFrame* pFrame = new CTodoFrame;
    CRootWindow::Instance().AddChild (pFrame);
    m_Doc.RegisterView (&m_Curlist);
    m_Curlist.RegisterView (pFrame);

    // Open the document.
    try {
	m_Doc.Open (fullname);
    } catch (...) {
	string msg (fullname);
	msg += " is corrupt or in an incompatible format.";
	MessageBox (msg);
	CRootWindow::Instance().Close();
	#ifndef NDEBUG
	    throw; // to see the real error for debugging.
	#endif
    }
}

//----------------------------------------------------------------------

WinMain (CTodoApp)

//----------------------------------------------------------------------

